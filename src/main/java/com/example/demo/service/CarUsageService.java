package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.CarUsageDTO;
import com.example.demo.entities.CarUsage;

public interface CarUsageService {

	long createCarUsage(CarUsage carUsage);

	String updateCarUsage(CarUsage car);

	String deleteCarUsage(String id);

	List<CarUsageDTO> findAllCars();

}
