package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.CarUsageDAO;
import com.example.demo.dto.CarUsageDTO;
import com.example.demo.entities.CarUsage;
import com.example.demo.service.CarUsageService;

@Service
public class CarUsageServiceImpl implements CarUsageService {

	@Autowired
	CarUsageDAO cuRepository;
	
	@Override
	public long createCarUsage(CarUsage carUsage) {
		// TODO Auto-generated method stub
		CarUsage c = this.cuRepository.save(carUsage);
		return c.getId(); 
	}

	@Override
	public String updateCarUsage(CarUsage car) {
		// TODO Auto-generated method stub
		CarUsage c = this.cuRepository.findOneByCarId(car.getCarId());
		if(c != null) {
			if(c.getId() == car.getId()) {
				BeanUtils.copyProperties(car, c);
				CarUsage c1 = this.cuRepository.save(c);
				return "car id " + c1.getId() + " is successfully updated";
			}
			return "car id is not matched";
		}
		return "car id is not available";
	}

	@Override
	public String deleteCarUsage(String id) {
		// TODO Auto-generated method stub
		List<CarUsage> cars = this.cuRepository.findAllByCarId(id);
		if(cars.isEmpty()) {
			return "This car id is not available";
		}else {
			cars.stream().forEach(car -> {
				this.cuRepository.deleteById(car.getId());
			});		
			return "Deleted successfully";
		}
	}

	@Override
	public List<CarUsageDTO> findAllCars() {
		// TODO Auto-generated method stub
		List<CarUsageDTO> cuDTOs = new ArrayList<>();
		List<CarUsage> cars = this.cuRepository.findAll();
		cars.stream().forEach(car -> {
			CarUsageDTO carDTO = new CarUsageDTO();
			BeanUtils.copyProperties(car, carDTO);
			cuDTOs.add(carDTO);
		});
		return cuDTOs;
	}

}
