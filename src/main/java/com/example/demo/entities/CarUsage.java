package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "car_usage")
public class CarUsage {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "car_id")
	private String carId;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "trip_start_timestamp")
	private Long tripStartTimestamp;

	@Column(name = "trip_end_timestamp")
	private Long tripEndTimestamp;
	
	@Column(name = "distance_covered")
	private double distanceCovered;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getTripStartTimestamp() {
		return tripStartTimestamp;
	}

	public void setTripStartTimestamp(Long tripStartTimestamp) {
		this.tripStartTimestamp = tripStartTimestamp;
	}

	public Long getTripEndTimestamp() {
		return tripEndTimestamp;
	}

	public void setTripEndTimestamp(Long tripEndTimestamp) {
		this.tripEndTimestamp = tripEndTimestamp;
	}

	public double getDistanceCovered() {
		return distanceCovered;
	}

	public void setDistanceCovered(double distanceCovered) {
		this.distanceCovered = distanceCovered;
	}
	
}
