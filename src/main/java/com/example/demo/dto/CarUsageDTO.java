package com.example.demo.dto;

public class CarUsageDTO {

	private String carId;
	
	private String city;
	
	private Long tripStartTimestamp;
	
	private Long tripEndTimestamp;
	
	private double distanceCovered;

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Long getTripStartTimestamp() {
		return tripStartTimestamp;
	}

	public void setTripStartTimestamp(Long tripStartTimestamp) {
		this.tripStartTimestamp = tripStartTimestamp;
	}

	public Long getTripEndTimestamp() {
		return tripEndTimestamp;
	}

	public void setTripEndTimestamp(Long tripEndTimestamp) {
		this.tripEndTimestamp = tripEndTimestamp;
	}

	public double getDistanceCovered() {
		return distanceCovered;
	}

	public void setDistanceCovered(double distanceCovered) {
		this.distanceCovered = distanceCovered;
	}
	
}
