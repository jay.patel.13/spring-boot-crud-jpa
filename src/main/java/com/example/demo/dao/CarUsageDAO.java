package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entities.CarUsage;

public interface CarUsageDAO extends JpaRepository<CarUsage, Long> {

	CarUsage findOneByCarId(String carId);

	List<CarUsage> findAllByCarId(String id);

}
