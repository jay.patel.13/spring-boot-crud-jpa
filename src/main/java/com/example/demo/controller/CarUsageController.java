package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CarUsageDTO;
import com.example.demo.entities.CarUsage;
import com.example.demo.service.CarUsageService;

@RestController
@CrossOrigin
@RequestMapping("/carUsage")
public class CarUsageController {

	@Autowired
	private CarUsageService cus;
	
	@PostMapping("/createCarUsage")
	  public long createCarUsage(@RequestBody CarUsage carUsage) throws Exception {
			return this.cus.createCarUsage(carUsage);
	  }
	
	@PostMapping("/updateCarUsage")
	  public String updateCarUsage(@RequestBody CarUsage car) throws Exception {
			return this.cus.updateCarUsage(car);
	  }
	
	@DeleteMapping("/deleteCarUsage")
	  public String deleteCarUsage(@RequestParam("carId") String id) throws Exception {
			return this.cus.deleteCarUsage(id);
	  }
	
	@GetMapping("/allCarUsage")
	  public List<CarUsageDTO> findAllCarUsage() throws Exception {
			return this.cus.findAllCars();
	  }
}
